
## 0.5.0 [06-04-2021]

* added 3 calls and added a forth with changed params

See merge request itentialopensource/adapters/controller-orchestrator/adapter-versa_director!7

---

## 0.4.1 [03-23-2021]

* add new call and fix body objects

See merge request itentialopensource/adapters/controller-orchestrator/adapter-versa_director!6

---

## 0.4.0 [03-19-2021]

* adding requested calls to the adapter

See merge request itentialopensource/adapters/controller-orchestrator/adapter-versa_director!5

---

## 0.3.2 [03-16-2021]

* final march migration to the latest adapter foundation

See merge request itentialopensource/adapters/controller-orchestrator/adapter-versa_director!4

---

## 0.3.1 [02-19-2021]

* make changes to importTemplateString

See merge request itentialopensource/adapters/controller-orchestrator/adapter-versa_director!3

---

## 0.3.0 [02-18-2021]

* added 2 calls including getTemplateFromOrganization

See merge request itentialopensource/adapters/controller-orchestrator/adapter-versa_director!2

---

## 0.2.0 [02-17-2021]

* Minor/adapt 419

See merge request itentialopensource/adapters/controller-orchestrator/adapter-versa_director!1

---

## 0.1.1 [10-20-2020]

* Bug fixes and performance improvements

See commit 96fa9d2

---
